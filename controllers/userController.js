const User = require("../models/user.js"); // returns objects, literal and function, not the whole document.
const Course = require ("../models/course.js")
const bcrypt = require("bcrypt"); // bcrypt - package for password hashing
const auth = require("../auth.js")

module.exports.checkEmailExist = (reqBody) => {

	// ".find - a mongoose CRUD operation (query) to find a field value from a collection"
	return User.find({email:reqBody.email}).then(result =>{
		// condition if there is an existing user
		if (result.length > 0) {
			return true;
		// condition if there is no existing user
		} else {
			return false;
		}
	})
}

module.exports.registerUser = (reqBody) => {
	
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		// bcrypt - package for password hashing
		// hashSync - synchronously gereate a hash
		password: bcrypt.hashSync(reqBody.password, 10),
		// 10 = salt rounds
		// Salt rounds is proportional to hashing rounds, the higher the salt rounds, the more hashing rounds.
		mobileNo: reqBody.mobileNo
	})

	return newUser.save().then((user, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	})
}

module.exports.loginUser = (reqBody) => {
	return User.findOne({email:reqBody.email}).then(result => {
		if (result == null) {
			return false;
		} else {
			// compareSync is bcrypt function to compare a hashed password to unhashed password
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				// if password does not match
				return false;
				// return "Incorrect password"
			}
		}
	})
}

module.exports.getProfile = (reqBody) => {

	return User.findOne({_id: reqBody._id}).then(result => {
		if (result == null) {
			return false;
		} else {
			result.password = "";
			return result;
		}
	})
}


module.exports.getAllDetails = () => {
	return User.find({}).then(result => {
		return result;
	})
}

// Enroll user to a class

module.exports.enroll = async (data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.enrollments.push({courseId : data.courseId});

		return user.save().then((user, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		})
	});

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		course.enrollees.push({userId : data.userId});

		return course.save().then((course, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		})
	});
	if (isUserUpdated && isCourseUpdated) {

		return true;
	} else {
		return false;
	}
};

