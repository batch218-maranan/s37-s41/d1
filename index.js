/*
GitBash:
npm init -y
npm install express
npm install mongoose
npm install cors
npm install bcrypt - used for password hashing
npm install jsonwebtoken - produce access web token to access features
*/

// const port = 4000;

/*const dotenv = require('dotenv');
dotenv.config();
const dbURL = process.env.DATABASE_URL;*/

// dependencies
const express = require ("express");
const mongoose = require ("mongoose");
const cors = require ("cors");


// ROUTERS
const userRoute = require ("./routes/userRoute.js");
const courseRoutes = require ("./routes/courseRoutes.js");


// to create a express server/application
const app = express();

// MIDDLEWARES - allows to bridge our backend application (server) to our front-end

// to allow cross origin resourse sharing
app.use(cors());
// to read JSON objects
app.use(express.json());
// to read forms
app.use(express.urlencoded({extended:true}));

app.use("/users", userRoute);
app.use("/courses", courseRoutes);


// Used to connect to our MongoDB Database
mongoose.connect("mongodb+srv://admin:admin@batch218-coursebooking.gxoljvg.mongodb.net/courseBooking?retryWrites=true&w=majority", {
    useNewUrlParser:true,
    useUnifiedTopology:true
    });

// Prompts a message once connected
mongoose.connection.once('open', () => console.log('Now connected to Maranan-Mongo DB Atlas'));

app.listen(process.env.PORT || 4000, () => 
{console.log(`API is now online on port ${process.env.PORT || 4000}`)
});
// 3000, 4000, 5000, 8000, - PORT numbers for web applications


