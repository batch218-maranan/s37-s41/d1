const express = require("express"); // accessing package of express
const router = express.Router(); // use dot notation to access content of package

const courseController = require("../controllers/courseController.js");
const auth = require("../auth.js");

//  Create a single course
router.post("/create", (req, res) => {
	courseController.addCourse(req.body).then(
		resultFromController => res.send(resultFromController))
})

router.get("/all", (req, res) => {
	courseController.getAllCourse().then(resultFromController => res.send(resultFromController))
})


router.get("/active", (req, res) => {
	courseController.getAllActiveCourses().then(resultFromController => res.send(resultFromController))
})

router.get("/:courseId", (req, res) => {
								// retrieves the ID from the URL
	courseController.getCourse(req.params.courseId).then(
		resultFromController => res.send(resultFromController))
})

// localhost4000:courseId/update - valid
// localhost4000:update.courseId - invalid
router.patch("/:courseId/update", auth.verify, (req, res) => {
	const newData = {
		course : req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	
	courseController.updateCourse(req.params.courseId, newData).then(
		resultFromController => {
			res.send(resultFromController)
		})
})

router.patch("", auth.verify, (req, res) => {
	const newData = {
		course : req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.createCourse(req.body).then(
		resultFromController => res.send(resultFromController))
})


router.patch("/:courseId", auth.verify, (req, res) => {
	const newData = {
		course : req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	
	courseController.archiveCourse(req.params.courseId, newData).then(
		resultFromController => {
			res.send(resultFromController)
		})
})



module.exports = router;