// dependencies
const express = require("express");
const router = express.Router();
const User = require("../models/user.js");
const userController = require("../controllers/userController.js");
const auth = require("../auth.js");

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExist(req.body).then(
		resultFromController => res.send(resultFromController))
});

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})

router.post("/details", (req, res) => {
	userController.getProfile(req.body).then(resultFromController => res.send(resultFromController))
})

router.get("/viewAllDetails", (req, res) => {
	userController.getAllDetails().then(resultFromController => res.send(resultFromController))
})

router.post("/enroll", auth.verify, (req, res) => {
	let data = {
				userId: req.body.userId, 
				courseId: req.body.courseId
				};

	userController.enroll(data).then(resultFromController => res.send(resultFromController))

})


module.exports = router;